package com.example.piotr.myweather;

import java.util.ArrayList;

/**
 * Created by Piotr on 2015-05-09.
 */
public class Items {

    private String pName;

    private ArrayList<SubCategory> mSubCategoryList;

    public Items(String pName, ArrayList<SubCategory> mSubCategoryList) {
        super();
        this.pName = pName;
        this.mSubCategoryList = mSubCategoryList;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public ArrayList<SubCategory> getmSubCategoryList() {
        return mSubCategoryList;
    }

    public void setmSubCategoryList(ArrayList<SubCategory> mSubCategoryList) {
        this.mSubCategoryList = mSubCategoryList;
    }
}