package com.example.piotr.myweather;

import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Piotr on 2015-05-10.
 */

import com.example.piotr.myweather.WeatherClass.*;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;


public class WeatherFragment extends Fragment {

    private String nazwaMiasta;
    private static double temp;
    private static double pressure;
    private static double humidity;


    public WeatherFragment() {
        //empty constructor
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public WeatherFragment(String nazwaMiasta) {
        this.nazwaMiasta = nazwaMiasta;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.weather_layout, container, false);
        TextView nazwaMiastaTextview = (TextView) view.findViewById(R.id.nazwaMiastaTxt);
        TextView pressureT = (TextView) view.findViewById(R.id.pressureTxt);
        TextView tempT = (TextView) view.findViewById(R.id.tempTxt);
        TextView humidityT = (TextView) view.findViewById(R.id.humidityTxt);
        ImageView imageView = (ImageView) view.findViewById(R.id.weatherIcon);

        nazwaMiastaTextview.setText(nazwaMiasta);
        final String link;
        link = "http://api.openweathermap.org/data/2.5/weather?q=" + nazwaMiasta;
        System.out.println("Link: " + link);
        String response = null;

        class doGet implements Runnable {
            public String returnVal;

            public void run() {
                GetHTTP getHTTP = new GetHTTP();
                returnVal = getHTTP.doInBackground(link);
            }

            public String getValue() {
                return returnVal;
            }
        }

        if(isNetworkAvailable()) {
            temp=0;
            humidity=0;
            pressure=0;

            doGet doGet = new doGet();
            Thread th1 = new Thread(doGet);
            th1.start();
            do {
                try {
                    th1.join(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (doGet.getValue() == null);
            response = doGet.getValue();

            Gson gson = new Gson();
            Example example = gson.fromJson(response, Example.class);
            Picasso.with(getActivity().getApplicationContext()).load("http://openweathermap.org/img/w/"+example.getWeather().get(0).getIcon()+".png").into(imageView);

            temp = (example.getMain().getTemp() - 273.15);
            pressure = example.getMain().getPressure();
            humidity = example.getMain().getHumidity();

//            Toast toast = Toast.makeText(getActivity().getApplicationContext(),"Pobrano pogodę",Toast.LENGTH_SHORT);
//            toast.show();

        }else{
            temp=0;
            humidity=0;
            pressure=0;
            Toast toast = Toast.makeText(getActivity().getApplicationContext(),"Brak dostępu do internetu",Toast.LENGTH_SHORT);
            toast.show();
            //jak niema neta
        }

        pressureT.setText("Cisnienie: " + String.format("%.2f",(float)pressure) + " hPa");
        tempT.setText("Temperature: " + String.format("%.3f",(float)temp) + "°C");
        humidityT.setText("Wilgotnosc: " + String.format("%.1f",(float)humidity) + "%");

        return view;
    }
}
